<?php
include "database.php";//db csatlakozás
//adatbázis lekérések a classicmodels adatbázisból
$qry = "SELECT 
            CONCAT(e1.firstname,' ',e1.lastname) alkalmazott, 
            IF(
                e2.firstName IS NULL,
                'BOSS', 
                CONCAT(e2.firstname,' ',e2.lastname)
            ) fonok
        FROM employees e1
        LEFT JOIN employees e2
        ON
         e1.reportsTo = e2.employeeNumber";//kérés kialakítása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés

$table = '<table border="1">';
//címsor
$table .= '<tr>
              <th>Alkalmazott</th>
              <th>Főnöke</th>
            </tr>';
while( $row = mysqli_fetch_assoc($result) ){
    $table .= '<tr>
                  <td>'.$row['alkalmazott'].'</td>
                  <td>'.$row['fonok'].'</td>
                </tr>';
    //echo '<pre>'.var_export($row,true).'</pre>';//kibontás ciklusban
}
$table .= '</table>';
//kiírás 1 lépésben

echo $table;

//@todo órai feladat: adattábla készítése a vásárlókból, módosít+töröl linkekkel kiegészítve
/*
Összes vásárló
Név	    contact neve(egyben)	telefon		alkalmazott neve(egyben)  	módosít | töröl
1. privát üzenet: SQL kérés
2. Ha kész beállítod a képernyőd, hogy csak a táblázat legyen, és szólsz, hogy kész.
 */

/*
 * @todo HF: Szobafoglalás adatainak tárolása adatbázisban
 * @todo Tábla terv készítése (mysql dev, vagy jegyzet mező tipusok)
 */
