<?php
//file betöltése - mintha a tartalma ide lenne gépelve
//include "functions.php";//ha nincs meg warning és tovább
include_once "functions.php";
//require "functions.php";//ha nincs meg a file, állj!
require_once "functions.php";// _once csak akkor tölti be ha még nem volt betöltve
if (!empty($_POST)) {
    echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];

    //név nem lehet üres
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//html elemek eltávolítása
    $name = trim($name);//space és egyéb alap elemek(\r\n...) eltávolítása
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező mező!</span>';
    }

    //email kötelező és email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {//ha nem jó email formátum
        $hiba['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }

    //jelszó, min 6 karakter és jelszo1 = jelszo2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, "utf-8") < 6) {
        $hiba['password'] = '<span class="error">A jelszó legalább 6 karakter!</span>';
    } elseif ($repassword !== $password) {
        $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek</span>';
    } else {
        /*
        //béna md5 kódolás (jelszónál ne használd!!!!)
        $password = md5($password.'!S3cr3t_K3y!');//md5 hash, nem visszafordítható kódolás algoritmus (elavult)
        */
        $password = password_hash($password, PASSWORD_BCRYPT);
        //var_dump($password);
    }

    //terms kötelező elfogadni
    $terms = filter_input(INPUT_POST, 'terms');
    if (!$terms) {
        $hiba['terms'] = '<span class="error">Kötelező bejelölni!</span>';
    }

    //ha nincs hiba
    if (empty($hiba)) {
        //adatok rendberakása
        $data = [
          'name' => $name,
          'email' => $email,
          'password' => $password
        ];
        //mikor történt a regisztráció
        $time_created = date("Y-m-d H:i:s");//php.net! (2021-04-29 19:32:15)
        //beletesszük a rendezett adataink közé:
        $data['time_created'] = $time_created;

        echo '<pre>' . var_export($data, true) . '</pre>';
        //echo $data;//nem kiírható ebben a formában, fileba se
        //(adat)tömb átalakítás 'szöveges formába' - tárolható
        //serialize
        echo $dataSerialized = serialize($data);
        //@todo HF: tárold el dataserialized.txt fileban
        //visszaalakítás
        echo '<pre>' . var_export( unserialize($dataSerialized), true) . '</pre>';

        //json
        echo $dataJson = json_encode($data);//szöveges
        //@todo HF2: tárold el data/user.json file-ba (data mappa!)
        //visszaalakítás
        echo '<pre>' . var_export( json_decode($dataJson, true), true) . '</pre>';
        die('<br>Oké');
    }
}


//php űrlap
$form = '<form method="post">';
//név
$form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" value="' . getValue('name') . '" placeholder="John Doe">' . getError('name') . '          
          </label>';
//email
$form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" value="' . getValue('email') . '" placeholder="email@you.com">' . getError('email') . '          
          </label>';

//jelszó
$form .= '<label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="password" value="" placeholder="******">' . getError('password') . '          
          </label>';

//jelszó újra
$form .= '<label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repassword" value="" placeholder="******">' . getError('repassword') . '          
          </label>';

//checkbox - adatvédelmi
$form .= '<label class="terms">
            <input type="checkbox" name="terms" value="1">
            <span>'.getError('terms').' Elolvastam és megértettem az <a href="#" target="_blank">Adatvédelmi Tájékoztató</a>ban leírtakat!</span>
          </label>';

$form .= '<button>küldés</button></form>';

//űrlap kiírása
echo $form;

//stílusok, ideiglenesen itt...
echo $style = '<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
form {
    max-width:400px;
    margin: 0 auto;
    display:flex;
    flex-direction: column;
}
label {
    display:flex;
    flex-direction: column;
    margin: 15px 0;
}
label.terms {
    flex-direction:row;
}
.error {
    color:#f00;
    font-style:italic;
    font-size:.8em;
}
</style>';
