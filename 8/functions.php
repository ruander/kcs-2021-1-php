<?php
//saját segéd eljárások (helpers)
/**
 * Input mezők value-át adja vissza a POST-ból, mezőnév alapján
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName)
{
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * INPUT elemek hibáinak kiírásához segédeljárás (helper)
 * @param $fieldName
 * @return string ->  hibaüzenet
 */
function getError($fieldName)
{
    global $hiba;//eljárás idejére a hiba globális lesz
    $ret = '';
    //var_dump($hiba);
    if (isset($hiba[$fieldName])) {//ha létezik az adott elem (hibaüzenet) eltároljuk ret -be
        $ret = $hiba[$fieldName];
    }

    return $ret;//majd visszatérünk a ret-el
}
