<?php
//saját segéd eljárások (helpers)

/**
 * login logika
 * @return bool
 */
function login()
{
    global $link;//lássuk a db csatlakozást
    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');
//lekérjük az emailhez tartozó jelszót
    $qry = "SELECT password,id,name FROM admins WHERE email = '$email' and status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
//ha van row (azaz talált email egyezést) akkor password check
    if ($row !== null && password_verify($password, $row[0])) {
        //echo 'SIKER!';
        //ekkor történt a belépés:
        $stime = time();//UNIX timestamp
        $sid = session_id();//mf azonosító
        //MF jelszó md5(sid+userId+SECRET)
        $spass = md5($sid . $row[1] . SECRET_KEY);
        $_SESSION['userdata'] = [
            'id' => $row[1],
            'username' => $row[2],
            'email' => $email
        ];
        $_SESSION['id'] = $sid;
        //takarítsunk, ne legyen 'beragadt' azonosító a belépéskor
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        //tároljuk el a belépést az adatbázisban
        $qry = "INSERT INTO sessions (`sid`,`spass`,`stime`) VALUES('$sid','$spass',$stime)";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        return true;
    }
    return false;
}
/**
 * Érvényes belépés ellenőrzése (azonosítás)
 * @return bool
 */
function auth(){
    global $link;//lássuk az eljárásban
    //$_SESSION,sessions táblában record,settings SECRET_KEY
    $now = time();
//öntisztítás
    $expired = $now - (60*15);//lejárt mf ok törlése (most 15 perc)
//töröljük a lejárt mf-okat
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
//lekérjük a sessionhöz tartozó recordot
    $sid = session_id();
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);//ha volt ilyen rekord akkor a spass a $row[0]-n található
    //var_dump($row);
//spass most alapján tesztelni tudunk:
//ha nincs [userdata][id] a sessionben akkor eleve hiba, ha van de nem egyeznek a spass értékek hiba
    if(
        isset($_SESSION['userdata']['id'])
        &&
        $row[0] ===  md5($_SESSION['id'] . $_SESSION['userdata']['id'] . SECRET_KEY)
    ) {
        //minden oké, FRISSITJUK AZ stime-OT
        mysqli_query($link, "UPDATE sessions SET stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;
    }
    //gond volt....
    return false;
}

/**
 * Logout
 * :void
 */
function logout(){
    //roncsolunk amit érünk :)
    global $link;
    mysqli_query($link, "DELETE FROM sessions WHERE sid = '{$_SESSION['id']}' LIMIT 1") or die(mysqli_error($link));
    $_SESSION = [];
    session_destroy();
}
/**
 * Input mezők value-át adja vissza a POST-ból, mezőnév alapján
 * @param $fieldName string
 * @param array $rowData | a db-ből lekért adatok tömbje
 * @return mixed
 */
function getValue($fieldName, $rowData = [])
{
    if(filter_input(INPUT_POST, $fieldName)){//ha van POST adat visszatérünk vele
        return filter_input(INPUT_POST, $fieldName);
    }
    if(array_key_exists($fieldName, $rowData)){//adatbázis adat ugyanlyan néven van-e
        return $rowData[$fieldName];
    }
    return false;
}

/**
 * INPUT elemek hibáinak kiírásához segédeljárás (helper)
 * @param $fieldName
 * @return string ->  hibaüzenet
 */
function getError($fieldName)
{
    global $hiba;//eljárás idejére a hiba globális lesz
    $ret = '';
    //var_dump($hiba);
    if (isset($hiba[$fieldName])) {//ha létezik az adott elem (hibaüzenet) eltároljuk ret -be
        $ret = $hiba[$fieldName];
    }

    return $ret;//majd visszatérünk a ret-el
}
//DB kezelő eljárások
/**
 * insert query string összeállítás adattömb segítségével
 * @param string $dbTable | tábla neve amibe az insert kell
 * @param array $data | beillesztendő adatok, a kulcsoknak egyeznie kell a fieldnevekkel
 * @return string
 */
function getInsertQuery($dbTable, $data = []){
    if(!is_array($data)){
        trigger_error('Nem jó paraméterek (getInsertQuery())!!!',E_USER_ERROR);
        //return false;
    }
    $qryFields = '`'.implode('`,`',array_keys($data)).'`';
    $qryValues = "'".implode("','",$data)."'";
    $qry = "INSERT INTO $dbTable($qryFields) VALUES($qryValues)";
    return $qry;
}
