<?php
const SECRET_KEY = 'wauedgcziasc134';//titkosító kulcs
const MODULE_DIR = 'modules/';//itt lesznek a modulok
const MODULE_EXT = '.php';//ilyen kiterjesztésűek a modul fájlok
const APP_URL = 'http://admin.cms.local/';//fő 'domain'
//menüpontok az adminhoz
const ADMIN_MENU = [
    0 => [
        'title' => 'Vezérlőpult',
        'icon' => 'fa fa-tachometer-alt',
        'module' => 'dashboard'
    ],
    1 => [
        'title' => 'Adminisztrátorok',
        'icon' => 'fa fa-user',
        'module' => 'admins'
    ],
    2 => [
        'title' => 'Szobafoglalás',
        'icon' => 'fa fa-user',
        'module' => 'reservations'
    ]
];
