<?php
require "../config/functions.php";
//db csatlakozás betöltése
require "../config/database.php";
//settings
require "../config/settings.php";
session_start();//mf indítása
//logout ha kell
if(filter_input(INPUT_GET,'logout') !== null){
    logout();
}
$o = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT)?:0;//ha nincs ilyen akkor 0 azaz dashboard
$baseURL = APP_URL.'index.php?p='.$o;//Alap url // modulokban is használatba vesszük!!!
$auth = auth();
if (!$auth) {
    //irány a login
    header('location:login.php');
    exit();
}
//$output
$output = '';
//modul betöltése ha van
$modulName = MODULE_DIR.ADMIN_MENU[$o]['module'].MODULE_EXT;
if(file_exists($modulName)){
    include $modulName;//moduleból $output néven kell jönni a kiírandónak
}else{
    $output = '<div>Nincs ilyen modul: '.$modulName.'</div>';
}


//userbar
$userbar = '<div class="userbar">Üdvözlet <b>'.$_SESSION['userdata']['username'].'</b> | <a href="?logout">kilépés</a></div>';


//modul betöltése, most fixen az admins
//include "modules/admins.php";
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztrációs felület</title>
</head>
<body>
<?php echo $userbar; ?>
<header>
    <?php
    $admin_menu= '<nav><ul>';
    //menüpontok
    foreach(ADMIN_MENU as $menuId => $menuItem){
        $admin_menu .= '<li><a href="?p='.$menuId.'">'.$menuItem['title'].'</a></li>';
    }
    $admin_menu .='</ul></nav>';

            echo $admin_menu;
    ?>
</header>
<main>
    <?php
    echo $output;//vagy modulból, vagy nincs ilyen modul üzenet
    ?>
</main>
<footer>lábléc</footer>
</body>
</html>
