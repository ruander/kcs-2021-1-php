<?php
require "../config/functions.php";
//db csatlakozás betöltése
require "../config/database.php";
require "../config/settings.php";//beállítások
//mf kezelése és $_SESSION
//var_dump($_SESSION);
session_start();//mf indítása (nem írhatsz ki stdoutra előtte semmit)
//var_dump($_SESSION, session_id());
$info = '';
if (!empty($_POST)) {
    if (login()) {
        //irány az index
        header('location:index.php');
        exit();
    } else {
        $info = '<div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-ban"></i> Hiba!</h5>
                  Nem megfelelő email/jelszó páros!
                </div>';
    }
}

?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ruander Oktatóközpont - CMS - Adminisztráció</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="https://ruander.hu" target="_blank" class="h1"><b>Ruander</b> Oktatóközpont</a>
            <br>
            Adminisztrácó (CMS)
        </div>
        <div class="card-body">
            <p class="login-box-msg">Irja be a belépési adatokat</p>
            <?php echo $info; ?>
            <form method="post">
                <div class="input-group mb-3">
                        <input class="form-control" type="text" name="email" value="<?php echo getValue('email'); ?>" placeholder="email@cim.hu">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" name="password" class="form-control" placeholder="Jelszó">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <!--<div class="icheck-primary">
                          <input type="checkbox" id="remember">
                          <label for="remember">
                            Remember Me
                          </label>
                        </div>-->
                        <a href="#forgot-password.html">Elfelejtett jelszó</a>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Belépés</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

            <!--<div class="social-auth-links text-center mt-2 mb-3">
              <a href="#" class="btn btn-block btn-primary">
                <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
              </a>
              <a href="#" class="btn btn-block btn-danger">
                <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
              </a>
            </div>-->
            <!-- /.social-auth-links -->

            <p class="mb-1">
                <!--<a href="#forgot-password.html">Elfelejtett jelszó</a>-->
            </p>
            <p class="mb-0">
                <!--<a href="register.html" class="text-center">Register a new membership</a>-->
            </p>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
</body>
</html>
