<?php
if (!isset($link)) {//önálló futtatás elleni védelem, mert az indexben töltődik be a db csatlakozás
    header('location:index.php');//menjünk az indexre
    exit();
}
//admins CRUD erőforrások
$dbTable = 'admins';//ebbe a táblába dolgozunk
$action = filter_input(INPUT_GET, 'action') ?: 'list';
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//tid kivel végzünk műveletet
$output = ''; //kiírandó elemek
if (!empty($_POST)) {
    //hibakezelés
    $hiba = [];

    //név nem lehet üres
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//html elemek eltávolítása
    $name = trim($name);//space és egyéb alap elemek(\r\n...) eltávolítása
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező mező!</span>';
    }

    //email kötelező és email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {//ha nem jó email formátum
        $hiba['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }
//jelszó, min 6 karakter és jelszo1 = jelszo2
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //csak akkor ellenőrzünk jelszót ha új felvitel
    if ($action == 'create' || $password != '') {


        if (mb_strlen($password, "utf-8") < 6) {
            $hiba['password'] = '<span class="error">A jelszó legalább 6 karakter!</span>';
        } elseif ($repassword !== $password) {
            $hiba['repassword'] = '<span class="error">A jelszavak nem egyeztek</span>';
        } else {
            /*
            //béna md5 kódolás (jelszónál ne használd!!!!)
            $password = md5($password.'!S3cr3t_K3y!');//md5 hash, nem visszafordítható kódolás algoritmus (elavult)
            */
            $password = password_hash($password, PASSWORD_BCRYPT);
            //var_dump($password);
        }
    }
    //státusz ha nincs, legyen 0
    $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT) ?: 0;


    //ha nincs hiba
    if (empty($hiba)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];


        if (isset($password)) {//akkor legyen azadotk közt, ha kell
            $data['password'] = $password;
        }
        if ($action == 'create') {
            //mikor történt a regisztráció
            $time_created = date("Y-m-d H:i:s");//php.net! (2021-04-29 19:32:15)
            //beletesszük a rendezett adataink közé:
            $data['time_created'] = $time_created;

            //echo '<pre>' . var_export($data, true) . '</pre>';
            $qry = getInsertQuery($dbTable, $data);
        } else {//update eset
            $time_updated = date("Y-m-d H:i:s");//php.net! (2021-04-29 19:32:15)
            //beletesszük a rendezett adataink közé:
            $data['time_updated'] = $time_updated;

            $qry = "UPDATE $dbTable SET 
                       name = '{$data['name']}', 
                       status = '{$data['status']}', 
                       email = '{$data['email']}', 
                       time_updated = '{$data['time_updated']}'
                       WHERE id = $tid LIMIT 1";
        }

        mysqli_query($link, $qry) or die(mysqli_error($link));
        header('location:' . $baseURL);//vissza listázásra
        exit();
    }
}

switch ($action) {
    case 'delete':
        if ($tid) {
            mysqli_query($link, "DELETE FROM $dbTable WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
        }
        header('location:' . $baseURL);//vissza listázásra
        exit();
        break;

    case 'update':
        //módosítás
        $qry = "SELECT name, email,status FROM $dbTable WHERE id = $tid LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_assoc($result);
        //var_dump($row);
        $title = "Módosítunk: $tid";

    //break;

    case 'create':
        //új felvitel
        //van e row? ha nincs legyen egy üres tömb
        if(!isset($row)){
            $row = [];
        }
        //php űrlap
        $title = isset($title) ? $title : 'Új felvitel';
        $form = '<h2>' . $title . '</h2><form method="post">';
//név
        $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" value="' . getValue('name', $row) . '" placeholder="John Doe">' . getError('name') . '          
          </label>';
//email
        $form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" value="' . getValue('email', $row) . '" placeholder="email@you.com">' . getError('email') . '          
          </label>';

//jelszó
        $form .= '<label>
            <span>Jelszó<sup>*</sup></span>
            <input type="password" name="password" value="" placeholder="******">' . getError('password') . '          
          </label>';

//jelszó újra
        $form .= '<label>
            <span>Jelszó újra<sup>*</sup></span>
            <input type="password" name="repassword" value="" placeholder="******">' . getError('repassword') . '          
          </label>';

//checkbox - státusz
        /** -A) ki kell pipálni ha:
         * -a) nincs post adat de row adat 1 (azaz van)
         * -b) van post adat és benn van a status elem
         * -B) nem kell kipipálni ha
         * -a)nincs semmi (se row adat se post adat)
         * -b) van row adat (0) van post de nincs benne az elem
         */
        //A)
        $checked = '';
        if (
            empty($_POST) //a)
            &&
            getValue('status', $row) //a)
            ||
            getValue('status') //b)
        ) {
            $checked = 'checked';
        }
        /*//B)
        $checked = 'checked';
        if (
            !empty($_POST) //b)
            &&
            !filter_input(INPUT_POST, 'status') //b)
            ||
            !getValue('status', $row) //a)
        ) {
            $checked = '';
        }*/
        $form .= '<label class="terms">
            <input type="checkbox" name="status" value="1" ' . $checked . '>
            <span> Státusz</span>
          </label>';

        $form .= '<button>küldés</button></form>';
        $output .= $form;
        break;

    default:
        //listázás CRUD táblázatba
        $output .= '<h2>Adminok lista</h2>';
        //listázás
        $qry = "SELECT id, name, email, status FROM $dbTable";//kérés kialakítása
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));//lekérés

        $table = '<div><a class="btn btn-primary mb-1" href="' . $baseURL . '&amp;action=create">új felvitel</a></div>
          <table class="table table-striped table-responsive">';
//címsor
        $table .= '<tr>
              <th>id</th>
              <th>név</th>
              <th>email</th>
              <th>státusz</th>
              <th>művelet</th>
            </tr>';
        while ($row = mysqli_fetch_row($result)) {
            $table .= '<tr>
                  <td>' . $row[0] . '</td>
                  <td>' . $row[1] . '</td>
                  <td>' . $row[2] . '</td>
                  <td>' . $row[3] . '</td>
                  <td><a href="' . $baseURL . '&amp;action=update&amp;tid=' . $row[0] . '">módosít</a> | <a onclick="confirm(\'Tuti biztos?\')" href="' . $baseURL . '&amp;action=delete&amp;tid=' . $row[0] . '">töröl</a></td>
                </tr>';
            //echo '<pre>'.var_export($row,true).'</pre>';//kibontás ciklusban
        }
        $table .= '</table>';
//kiírás 1 lépésben

        $output .= $table;//ez legyen a kimenete a filenak amit majd ki akarunk írni
        break;
}

//echo $output;//kiírandók kiírása
