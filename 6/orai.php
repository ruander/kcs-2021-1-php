<?php
//generálj egy tömbbe 5 számot 1-90 ig, egy szám csak 1x szerepelhet pl: [5,3,76,45,34]

/**
 * Tippsor(sorsolás) generáló eljárás
 * @param int $huzasok_szama
 * @param int $limit
 * @return array
 */
function sorsolas($huzasok_szama = 5, $limit = 90)//default érték a paramétereknek
{
    $tomb = [];
    //védelem
    // $limit legalább annyi kell legyen mint a huzások száma
    if($limit<$huzasok_szama){//hibás eljáráshívás( paraméterek)
        //echo 'Gáz van';
        trigger_error('Hibás paraméterezés a sorsolas eljárásban' , E_USER_ERROR);
        //return $tomb;//üres tömbbel térünk vissza
    }
    //generálás
    while (count($tomb) < $huzasok_szama) {
        $tomb[] = rand(1, $limit);
        $tomb = array_unique($tomb, SORT_REGULAR);//kiszedjük az esetlegesen ismétlődő eleme(ke)t így nem nőtt a tömb elemszáma
    }
    sort($tomb);//emelkedő sorrend
    //visszatérünk a generált tömbbel
    return $tomb;
}

$tomb = sorsolas();
echo '<pre>' . var_export($tomb, true) . '</pre>';

