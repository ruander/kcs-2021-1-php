<?php
    $output = '';//ide gyűjtjük a kiírandó elemeket
if (!empty($_POST)) {//feldolgozás
    var_dump($_POST);
    $hiba = [];//ide gyűjtjük a hibá(ka)t
    //hibakezelés
    //szűrő opció 1-90-hez
    $options = [
        "options" => [
            "min_range" => 1,
            "max_range" => 90
        ]
    ];
    $n = filter_input(INPUT_POST, 'n', FILTER_VALIDATE_INT, $options);
    var_dump($n);
    if(!$n){
        $hiba['n'] = '<span class="error">Hibás formátum!</span>';
    }
    /*if ($n < 1 or $n > 90) {
        $hiba['n'] = '<span class="error">Hibás formátum!</span>';
    }*/
    if (empty($hiba)) {
        //nincs hiba
        $output .= '<h2>Ügyes vagy!</h2>';
        $output .= '<a href="?">újra?</a>';
    }
}
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>űrlap 2</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            width: 450px;
            margin: 0 auto;
            flex-direction: column;
        }

        input {
            display: block;
        }

        .error {
            color: #f00;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<?php
//ha van output tartalom írjuk ki, különben a formot
if($output != ''){
    echo $output;
}else{
    //echo "Űrlap";
?>
<form method="post">
    <label>
        <span>Írj be egy 1-90 közötti egész számot<sup>*</sup></span>
        <input type="text" name="n" placeholder="12" value="<?php echo getValue('n'); ?>">
        <?php
        //hibaüzenet kiírása, ha van
        /*if(isset($hiba['n'])){
            echo $hiba['n'];
        }*/
        echo getError('n');//hiba kiírása saját eljárással
        ?>
    </label>
    <button>Küldés</button>
</form>
<?php
}
 ?>
</body>
</html><?php

/**
 * Input mezők value-át adja vissza a POST-ból, mezőnév alapján
 * @param $fieldName
 * @return mixed
 */
function getValue($fieldName)
{
    return filter_input(INPUT_POST, $fieldName);
}

/**
 * INPUT elemek hibáinak kiírásához segédeljárás (helper)
 * @param $fieldName
 * @return string ->  hibaüzenet
 */
function getError($fieldName)
{
    global $hiba;//eljárás idejére a hiba globális lesz
    $ret = '';
    //var_dump($hiba);
    if (isset($hiba[$fieldName])) {//ha létezik az adott elem (hibaüzenet) eltároljuk ret -be
        $ret = $hiba[$fieldName];
    }

    return $ret;//majd visszatérünk a ret-el
}
//@todo: HF (hardcore) - kérj be 5 számot, nem lehet köztük egyforma
//hibás formátum és "már szerepelt/ ismétlődő elem"
//@todo: HF (softcore) - kötelező Név/email/jelszó 2x/adatvédelmi - név, min 3 karakter, email kötelező(és email!) - két jelszó egyezzen, min 6 karakter, adatvédelmit kötelező kipipálni - segítség-> adatvédelmi nem kell h kipipálva maradjon hibás form esetén + jelszó mezőkbe SOHA nem írunk vissza semmit!!!!!!!!
