<?php
require_once "functions.php";// _once csak akkor tölti be ha még nem volt betöltve
//erőforrások
/*
     * szolgáltatások ÁLLANDÓ TÖMB
    `id` => [
        'name' => 'szauna',
        'price' => 1900
    ]
*/
const VALID_SERVICES = [
    0 => [
        'name' => "szauna",
        'price' => 1900
    ],
    1 => [
        'name' => "uszoda",
        'price' => 2900
    ],
    2 => [
        'name' => "wellness (szauna+uszoda)",
        'price' => 3490
    ],
    3 => [
        'name' => "várlátogatás kisérővel",
        'price' => 11900
    ],
    4 => [
        'name' => "borkostolás",
        'price' => 2700
    ],
    5 => [
        'name' => "mosoda",
        'price' => 900
    ]
];//szolgáltatások segédtömb (erőforrás)

//mappa ellenőrzés/létrehozás
$dir = 'foglalasok/';
if(!is_dir($dir)){
    mkdir($dir, 0755, true);
}


if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];

    //név nem lehet üres
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//html elemek eltávolítása
    $name = trim($name);//space és egyéb alap elemek(\r\n...) eltávolítása
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező mező!</span>';
    }

    //email kötelező és email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {//ha nem jó email formátum
        $hiba['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }
    //telefon, kötelező (text vagy tel)
    //@TODO ha nem küldted, HF!

    //érkezés,indulás (éjszakák száma)
    //érkezés hiba, ha ...
    //korábbi, mint az aktuális nap (ma)
    $arrival_date = filter_input(INPUT_POST, 'arrival_date');//éééé-hh-nn
    //dátum ellenőrzése 1: dátum-e?
    //kapott 'szöveg' timestampre(int) alakítása

    $now = date('Y-m-d');//ma
    /* echo $a_timestamp = strtotime($arrival_date);//időbélyeggé alakítjuk
     $a_day = date('d', $a_timestamp);
     $a_month = date('m', $a_timestamp);
     $a_year = date('Y', $a_timestamp);
     var_dump($a_month,$a_day,$a_year);
     //var_dump('datecheck:',checkdate( null , null ,2022));
     if (
     !checkdate($a_month, $a_day, $a_year)
         ||
         $now > $arrival_date
     ){
     $hiba['arrival_date'] = '<span class="error">Hibás adat (minimum mai nap)!</span>';
 }*/
    //dátum ellenőrzése 2: dátum-e?
    $date_elements = explode('-', $arrival_date);
    //var_dump($date_elements);
    if (
        count($date_elements) < 3 //elvileg ilyen nem lehet (form manipulálás)
        ||
        !checkdate($date_elements[1], $date_elements[2], $date_elements[0])
        ||
        $now > $arrival_date
    ) {
        $hiba['arrival_date'] = '<span class="error">Hibás adat (minimum mai nap)!</span>';
    }
    //távozás dátuma leave_date legyen legalább 1 nappal nagyobb mint az arrival_date
    $leave_date = filter_input(INPUT_POST, 'leave_date');//éééé-hh-nn
    if ($leave_date <= $arrival_date) {
        $hiba['leave_date'] = '<span class="error">Hibás adat (minimum érkezés +1 nap)!</span>';
    }
    //felnőttek, gyerekek száma kötelező, minimum 1 felnőtt
    $options = [
        'options' => [
            'min_range' => 1
        ]
    ];
    $adults = filter_input(INPUT_POST, 'adults', FILTER_VALIDATE_INT, $options);
    if (!$adults) {
        $hiba['adults'] = '<span class="error">Hibás adat (minimum 1 felnőtt)!</span>';
    }
    //gyermekek száma
    $children = filter_input(INPUT_POST, 'children', FILTER_VALIDATE_INT);
    if (
        !$children && $children !== 0 //nem sikerült leszűrni
        ||
        $children < 0 //vagy kisebb mint 0
    ) {
        $hiba['children'] = '<span class="error">Hibás adat!</span>';
    }

//ellátás kötelező (select-option)
//@TODO ha nem küldted, HF!

//szolgáltatások rendberakása
    $services = filter_input(INPUT_POST, 'services', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    //echo '<pre>' . var_export($services, true) . '</pre>';

//terms kötelező elfogadni
    $terms = filter_input(INPUT_POST, 'terms');
    if (!$terms) {
        $hiba['terms'] = '<span class="error">Kötelező bejelölni!</span>';
    }

//ha nincs hiba
    if (empty($hiba)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'arrival_date' => $arrival_date,
            'leave_date' => $leave_date,
            'adults' => $adults,
            'children' => $children,
            'services' => $services
        ];
        //mikor történt a regisztráció
        $time_created = date("Y-m-d H:i:s");//php.net! (2021-04-29 19:32:15)
        //beletesszük a rendezett adataink közé:
        $data['time_created'] = $time_created;

        //echo '<pre>' . var_export($data, true) . '</pre>';
        $jsonData = json_encode($data);
        //echo $data;//nem kiírható ebben a formában, fileba se
        $filename = time().'.json';
        file_put_contents($dir.$filename, $jsonData);
        //die('<br>Oké');
        //átirányítunk üres formra
        header('location:szobafoglalas.php');//header előtt nem írhatsz std outputra
        exit();//állj , ugyanaz mint a die
    }
}


//php űrlap
$form = '<form method="post">';
//név
$form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" value="' . getValue('name') . '" placeholder="John Doe">' . getError('name') . '          
          </label>';
//email
$form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" value="' . getValue('email') . '" placeholder="email@you.com">' . getError('email') . '          
          </label>';

//érkezés/távozás @todo HF: hogy tudunk pirossal jelölni, vagy inaktívvá tenni egyes napokat
$form .= '<label>
            <span>Érkezés<sup>*</sup></span>
            <input type="date" name="arrival_date" value="' .
    (getValue('arrival_date') ?: date('Y-m-d'))
    . '">' . getError('arrival_date') . '</label>';

$form .= '<label>
            <span>Távozás<sup>*</sup></span>
            <input type="date" name="leave_date" value="' . getValue('leave_date') . '">' . getError('leave_date') . '</label>';

//felnőttek/gyerekek száma
$form .= '<label>
            <span>Felnőttek száma<sup>*</sup></span>
            <input type="text" name="adults" value="' . getValue('adults') . '" placeholder="2">' . getError('adults') . '          
          </label>';

$form .= '<label>
            <span>Gyermekek száma 6-18 éves korig</span>
            <input type="text" name="children" value="' . getValue('children') . '" placeholder="2">' . getError('children') . '
            </label>';

//szolgáltatások

$form .= '<div class="services">';
$form .= '<h3>Válasszon a szolgáltatásaink közül</h3>';

////segédtömb($services) bejárásával
$services = filter_input(INPUT_POST, 'services', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
foreach (VALID_SERVICES as $serviceId => $service) {
    /*$checked = '';//alapértelmezett érték (üres)
    if(
        is_array($services)
        && //operátor: &&-> and || -> or
        array_key_exists($serviceId, $services)
    ){
            $checked = 'checked';
    }*/
    //short if
    $checked = is_array($services) && array_key_exists($serviceId, $services) ? 'checked' : '';

    $form .= '<label>
    <input type="checkbox" name="services[' . $serviceId . ']" value="1" ' . $checked . '> 
    ' . $service['name'] . ' 
    <span class="price">(' . $service['price'] . '.-HUF)</span>
    </label>';
}


$form .= '</div>';

//checkbox - adatvédelmi
$form .= '<label class="terms">
            <input type="checkbox" name="terms" value="1">
            <span>' . getError('terms') . ' Elolvastam és megértettem az <a href="#" target="_blank">Adatvédelmi Tájékoztató</a>ban leírtakat!</span>
          </label>';

$form .= '<button>küldés</button></form>';

//űrlap kiírása
echo $form;

//stílusok, ideiglenesen itt...
echo $style = '<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
form {
    max-width:400px;
    margin: 0 auto;
    display:grid;
    flex-direction: column;
}
label {
    display:flex;
    flex-direction: column;
    margin: 15px 0;
}
label.terms {
    flex-direction:row;
}
.error {
    color:#f00;
    font-style:italic;
    font-size:.8em;
}
</style>';

