<?php
//ha van valami a postban, akkor feldolgozzuk
/*if( empty($_POST) === false ){
    echo '<pre>'.var_export($_POST,true).'</pre>';
}
if( empty($_POST) !== true ){
    echo '<pre>'.var_export($_POST,true).'</pre>';
}*/
if( !empty($_POST) ){//! operátor -> negálás
    echo '<pre>'.var_export($_POST,true).'</pre>';
    //hibakezelés 1.
    $hiba = [];//üres tömb, ide gyülnek a hibák

    $name = filter_input(INPUT_POST,'name');
    //szöveg 'szélein' találhato spacek eltávolítása
    $name = trim($name);//balról -> ltrim(), jobbról-> rtrim()
    $hossza = mb_strlen($name,"utf-8");//strlen($name);
    if($hossza < 3){
        //hiba esetén az adott indexre helezzük a hibaüzenetet
        $hiba['name'] = '<span class="error">Min. 3 karakter!</span>';
    }

    //Email mező legyen email formátum
    $email = filter_input(INPUT_POST,'email', FILTER_VALIDATE_EMAIL);
    //email ha jó akkor a email string az értéke, ha nem létezik email elem akkor NULL ha nem email  fomrátum akkor false lesz NULL nak az értéke false a tipusa nem BOOLEAN ->
    // NULL == false igaz, de a NULL === false nem igaz
    if($email == false){
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }
    var_dump($hiba);
    //password

    //ha üres maradt a hibatömb akkor nincs hibám
    if( empty($hiba) ){
        die('nincs hiba');//megáll a kód futása, utolsó standard output stringként lehet még kiírni bármit.
    }//nincs hiba vége
}//!empty post vége
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap adatok feldolgozása azonos fileban</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        form, label {
            display:flex;
            flex-flow: column nowrap;
        }
        .error {
            color:#f00;
            font-style: italic;
            font-size: .7em;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        <span>Név<sup>*</sup> (min. 3 karakter)</span>
        <input type="text" name="name" placeholder="Nameless One" value="<?php echo filter_input(INPUT_POST,'name'); ?>">
        <?php
        //ha létezik hibaüzenet, írjuk ki
        if( isset($hiba['name']) ){
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        <span>Email<sup>*</sup></span>
        <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo filter_input(INPUT_POST,'email'); ?>">
        <?php
        //ha létezik hibaüzenet, írjuk ki
        if( isset($hiba['email']) ){
            echo $hiba['email'];
        }
        ?>
    </label>

    <button>Küldés</button>
</form>
</body>
</html>
