<?php
//PURE PHP file

//változók a PHP-ban
/*
$valtozoneve
$valtozoneve1
$valtozo_neve_1 | snake_case
$valtozoNeve_2 | camel case
 */
//primitivek
$egesz_szam = 123;// integer (int)
$lebego_pontos = 7/6;// floating point number - (float)
$szoveg = 'Horváth György';// string (str)
$logikai = true; //boolean (bool)

//információk a változórol 1 - csak fejlesztés közben!!!
var_dump($egesz_szam,$lebego_pontos,$szoveg,$logikai);

//tömbök

$tomb = [];//üres tömb
echo var_export($tomb,true);//stringként tér vissza a var_export ezért ki kell írni
//echo $tomb;// tömb nem primitív string, ezért nem írható ki echoval, sem print-el
$tomb = [1342, 43/13, false, "hgy@iworkshop.hu"];//tömb felvétele értékekkel
echo '<pre>';
//var_dump($tomb);
echo var_export($tomb, true);
echo '</pre>';
//egy lépésben string fűzéssel - konkatenáció
//echo '<pre>' . var_export($tomb, true) . '</pre>';// operátor: . -> konkatenáció
//tömb bővítése értékkel
$tomb[] = "új érték...";
echo '<pre>' . var_export($tomb, true) . '</pre>';//egy dimenziós tömb
//tömb bővítése értékkel - irányított indexre
$tomb[100] = 'Ez a 100 index';
$tomb['asszociativ_index'] = 'érték';
$tomb[] = "új érték...";
echo '<pre>' . var_export($tomb, true) . '</pre>';
//egy elem a tömbből
var_dump($tomb[100], $tomb['asszociativ_index']);
//tömb elemeszáma
echo '<br>A tömb elemszáma: ' . count($tomb);
//'beszédes változók'
$userdata = [
  'id' => 5,
  'name' => "Horváth György",
  'email' => "hgy@iworkshop.hu"
];
echo '<pre>' . var_export($userdata, true) . '</pre>';
echo 'Felhasználónév: ' . $userdata['name'];

//tipuskényszerítés objektummá
$objektum = (object) $tomb;
echo '<pre>';
var_dump($objektum);//objektum orientált PHP
echo '</pre>';

//Állandók
const G = 9.81;
const PI = 3.14;
const RENDSZER_ALLANDO = 42;
const VALID_IMAGE_TYPES = ['image/jpeg', 'image/jpg', 'image/png'];

var_dump(G,PI, RENDSZER_ALLANDO, VALID_IMAGE_TYPES);
//G = 10;//állandót TILOS felülírni/megVÁLTOZTATNI!!!
