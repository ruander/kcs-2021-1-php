<?php
/*
dobjunk egy kockával (1-6)
és mondjuk meg mit dobtunk és az páros vagy páratlan
A dobás érték 5(1-6), ami páratlan(páros).

Folyamat:
-generáljunk egy 1-6 közötti egész számot -> $dobas
-osszuk 2-vel -> maradék
maradék: 0 -> string + $dobas + páros
maradék: nem 0 -> string + $dobas + páratlan
*/
$dobas = rand(1,6);
$maradek = $dobas%2;//0 vagy 1
/*
if(condition){
    //igaz ág
}else{
    //hamis ág
}
*/
if($maradek == 0){// == érték egyezés vizsgálat, === érték ÉS tipusegyezés vizsgálat 5 == "5" -> igaz | 5 === "5" ->hamis
    //igaz ág maradék:0 (páros)
    echo 'A dobás értéke: ' . $dobas . ', ami páros.';
}else{
    echo "A dobás értéke: $dobas, ami páratlan.";
}
//refaktor 1
$output = '<div>A dobás értéke: ';//kiírandó
$output .= $dobas . ', ami ';//$output = $output . $dobas . ', ami ';//analógia: .=,+=,-=,*=
//echo $output;
if($dobas%2 == 0){
    $output .= 'páros.';//outputhoz fűzzük hogy páros
}else{
    $output .= 'páratlan.';//outputhoz fűzzük hogy páratlan
}
//div zárása
$output .= '</div>';
//kiírás egy lépésben (kiírandó -output)
echo $output;
//refaktor 2. - shorten if
/*
condition ? true : false
*/
//dobás már létezik
echo "<div>A dobás értéke: $dobas, ami " . ( $dobas%2 == 0 ? 'páros.' : 'páratlan.') . '</div>';
