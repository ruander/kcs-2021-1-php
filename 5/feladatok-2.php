<?php
/*
 19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
2
22
222
2222
 */
//ciklus a soroknak
for ($a = 1; $a <= 4; $a++) {
    //2es kiírása
    //belső ciklus a 2esek kiírására
    for ($b = 1; $b <= $a; $b++) {
        echo "2";
    }
    echo "<br>";
}
//ua. eljárással
for ($a = 1; $a <= 4; $a++) {
    //(belső ciklus a 2esek kiírására)
    echo str_repeat("2", $a) . "<br>";
}

//20. feladat
for ($a = 4; $a >= 1; $a--) {
    //(belső ciklus a 2esek kiírására)
    echo str_repeat("2", $a) . "<br>";
}

/*
21.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd kiírja a tömb legkisebb és legnagyobb elemét.
 */
$tomb = [];
for ($a = 0; $a < 10; $a++) {
    $tomb[] = mt_rand();
}

echo "<pre>" . var_export($tomb, true) . "</pre>";
echo "A legkisebb: " . min($tomb);
echo "<br>A legnagyobb: " . max($tomb);

//23.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, majd a tömb elemeit átmásolja egy másik tömbbe úgy, hogy az elemek fordított sorrendben helyezkedjenek el benne.
//$tomb = array_fill(0,10, "adasd");


//fordítottja:
$tomb2 = array_reverse($tomb);
echo "<pre>" . var_export($tomb2, true) . "</pre>";

//24.Írjon egy programot, amelyben egy 10 elemű tömböt tetszőleges értékekkel tölt fel, amelyek valamilyen termékek nettó árai, majd számolja ki és írassa ki a termékek bruttó árait, ha az ÁFA tartalom 20 %.
$netto = [150,234,500,330,3420,15600,6700,30,450,980];

foreach($netto as $ar){
    $brutto = $ar/0.8; // :)
    echo "<br>Netto: $ar.- HUF | Brutto : $brutto.- HUF (20% ÁFA tartalom)";
}

//+1 Írd ki a prímszámokat 1000 és 2000 között
//prím csak 1el és önmagával osztható

$number = 1001;
$is_prim = "igen";
//number prim-e
for($i=2;$i<=$number/2;$i++){
    if($number%$i == 0){
        $is_prim = "nem";
        //ha találtunk akár 1-et akkor felesleges továbbmenni
        break;
    }
}
echo "<h2>$number : $is_prim</h2>";

//saját eljárás készítése
/*
function eljarasNeve(param1,param2,...) {
    //...

//vagy visszatérünk 1 értékkel (bármi) return ...
//vagy "teszi a dolgát" :void
}
 */
var_dump(is_prime($number));
//feladat végső megoldása
for($x=1000;$x<=2000;$x++){//vizsgálandó számok ciklusa
    if(is_prime($x)){
        echo "<h2>$x, prím szám</h2>";
    }
}


/**
 * Prímszám vizsgálat
 * @param $n :int
 * @return bool
 */
function is_prime($n){
    $ret = true;
    for($i=2;$i<=$n/2;$i++){
        if($n%$i==0){
            //return false
            $ret = false;
            break;
        }
    }
    return $ret;
}
