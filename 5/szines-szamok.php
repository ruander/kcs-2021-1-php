<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Színes számok</title>
</head>
<body style="background: #ccc;">
<span style="color:#f00;">TESZT</span>
<p>Feladat: számok kírása 1-1000 ig
<br>
    2-vel oszthatóak legyenek pirosak
    <br>3-mal oszthatóak legyenek zöldek
    <br>4-gyel oszthatóak legyenek kékek
    <br>+ha egy szám több (2,3,4) értékkel is osztható akkor legyen a szám színe a keverék szín
</p>
<?php
    for($i=1;$i<=1000;$i++){
        $red=$green=$blue=0;
        //2
        if($i%2==0) $red=255;
        //3
        if($i%3==0) $green=255;
        //4
        if($i%4==0) $blue=255;

        echo "<h2 style='color:rgb($red,$green,$blue)'>$i</h2>";
    }
    /*@todo: HF - Űrlap készítése:
        név, email, jelszó 2x, checkbox
    PHP űrlap legyen, azaz a mezők egy-egy változóban tárolt string legyenek.
    */
?>
</body>
</html>
