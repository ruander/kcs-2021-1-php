<?php
//csatlakozás adatbázishoz
//adatok
$DBhost = 'localhost';
$DBuser = 'root';
$DBpassword = '';
$DBname = 'classicmodels';

//csatlakozás felépítése
$link = @mysqli_connect($DBhost,$DBuser,$DBpassword,$DBname) or die('GEBASZ: '.mysqli_connect_error());
//ha ide eljutunk, biztosan van $link DB kapcsolatunk

//kérés összeállítása
$qry = "SELECT * FROM employees";
//lekérdezés futtatása és eredmény tárolása
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//objektum -> resource
var_dump($result);
//eredmények kibontása (1 eredmény)
$row = mysqli_fetch_assoc($result);//asszociativ tömb
echo '<pre>'.var_export($row,true).'</pre>';
echo $row['firstName'].' '.$row['lastName'];
//eredmények kibontása (1 eredmény)
$row = mysqli_fetch_row($result);//nem asszociativ tömb
echo '<pre>'.var_export($row,true).'</pre>';

//eredmények kibontása (1 eredmény)
$row = mysqli_fetch_array($result);//nem asszociativ tömb
echo '<pre>'.var_export($row,true).'</pre>';

//eredmények kibontása (összes)
$rows = mysqli_fetch_all($result,MYSQLI_ASSOC);//nem asszociativ tömb
echo '<pre>'.var_export($rows,true).'</pre>';


//ha befejeztük db műveleteket, lezárjuk a nyitott DB kapcsolatot
mysqli_close($link);
