<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>PHP alap feladatok - gyakorlás</title>
</head>
<body>
<h1>PHP alap feladatok</h1>
<section>
    <h2>14.Írjon egy programot, amely kiszámolja az 1-től 10-ig terjedő egész számok átlagát ciklus utasítás használatával.</h2>
    <?php
    $sum = 0;//ide gyüjtjük a számok összegét
    for($i=1;$i<=10;$i++){
        $sum = $sum + $i;
    }
    //$i most 11
    $atlag = $sum/($i-1);

    //válasz
    $output = "<div>A számok átlaga 1-10ig: $atlag</div>";
    echo $output;//válasz kiírása
    ?>

</section>
</body>
</html>
