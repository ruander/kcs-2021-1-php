<?php
//elágazások 2 - switch
/*
 switch(változó){

    case 'érték':
            ...kód
        break;
    case 'érték2':
            ...kód
        break;
    case 'érték3':
            ...kód
        break;
    default:
            ...kód
        break;
}
 */
$test = 2;

switch ($test) {
    case 1:
        echo 'a test változó értéke most \\\'1\' ';//operátor: \ ->escape kilépteti a nyelvi elemek végrehajtásából a következő karaktert
        break;
    case 2:
        echo "a \$test változó értéke '2' ";
        break;
    default:
        echo 'egyik eset sem... default';
        break;
}

//switch 2 - hiányzó break bemutatása
$talalatok = rand(0, 5);

//nyereményosztály meghatározása
$output = "Találatok száma: $talalatok<br>";
switch ($talalatok) {
    case 5 :
        $output .= 'Gratulálunk: telitalálat! Főnyeremény!';
        //break;
    case 4 :
        $output .= 'III. nyerőosztály nyereménye...';
        //break;
    case 3 :
        $output .= 'II. nyerőosztály nyereménye...';
        break;
    case 2 :
        $output .= 'I. nyerőosztály nyereménye...';
        break;
    default:
        $output .= 'Nincs nyeremény...';
        break;
}
echo '<div>'.$output.'</div>';//'összegyűjtött' string kiírása

