<?php
//Készítsünk programot, amely kiszámolja az első 100 darab. természetes szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
$sum = 0;
for ($i = 1; $i <= 100; $i++) {
    $sum += $i;
}
$output = "<p>A pozitív egész számok összege 1-100ig: $sum</p>";
//válasz kiírása
echo $output;

//Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)
$mtpl = 1;
for ($i = 1; $i <= 7; $i++) {
    $mtpl *= $i;
}
$output = "<p>A pozitív egész számok szorzata 1-7-ig: $mtpl</p>";
//válasz kiírása
echo $output;

//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
$menuElemek = [
    'Kezdőlap', 'Rólunk', 'Szolgáltatások', 'Hírek', 'Kapcsolat'
];//menüpontok (0,1,2,3 kulcsokon)
$menu = '<nav class="myMenu">
            <ul>';
//menüpontok beillesztése
foreach ($menuElemek as $k => $v) {
    //var_dump($k,$v);
    $menu .= '<li><a href="?menu=' . $k . '">' . $v . '</a></li>';
}
$menu .= '</ul>
        </nav>';
echo $menu;
