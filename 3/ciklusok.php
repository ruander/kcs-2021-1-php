<?php
//ismétlődő programrész - ciklus
/*
for(ciklusváltozó kezdeti értéke;belépés feltétel vizsgálata;ciklusváltozó léptetése){
    ...programkód (cilkusmag)
}
 */
for($i=1; $i<=10; $i++){
    //ciklusmag
    echo '<br>'.$i;
}//ciklus vége
echo 'Az i értéke most:'.$i;

/*
 * előltesztelő
while(belépési feltétel viszgálata){
    ...programkód (cilkusmag)
}
 */
$i=1;
while($i<10){
    //ciklusmag
    echo '<br>'.$i;
    //ciklusváltozó léptetése
    $i++;
}
/*
 * hátultesztelő - (egyszer mindenképpen lefut)
do{
    ...programkód (cilkusmag)
}while(belépési feltétel viszgálata)
 */
$i=1;
do{
  echo '<br>Ezt mindenképp egyszer...';
  $i++;
}while($i<10);

//tömb elemeinek elérése - minden elem (ciklus)
$test = [
    'id' => 3,
    'name' => 'Nameless One',
    'email' => 'no@where.com'
];
/*
foreach($tombneve as $key => $value ){
    //ciklusmag ($key és $value elérhető)
}
*/
//tömb 'bejárása'
foreach($test as $k => $v){
    //ciklusmag
    echo "<br>Az aktuális kulcs(index): $k | érték: $v";
}
//Órai feladat
//tölts fel egy 10 elemű tömböt 1-100 közötti véletlen számokkal
$szamok = [];
//....while és tömb elemszám
while(count($szamok) < 10){
    $szamok[] = rand(1,100);
}
echo '<pre>'.var_export($szamok,true).'</pre>';
$szamok = [];
//....for és ciklusváltozó
for($i=0;$i<10;$i++){
    $szamok[] = rand(1,100);
}
echo '<pre>'.var_export($szamok,true).'</pre>';

//....while és ciklusváltozó
$szamok = [];
$i = 0;
while($i<10){
    $szamok[] = rand(1,100);
    $i++;
}
echo '<pre>'.var_export($szamok,true).'</pre>';

//...for és tömb elemszám
$szamok = [];
for(/*$i=0*/;count($szamok) < 10;/*$i++*/){
    $szamok[] = rand(1,100);
}
echo '<pre>'.var_export($szamok,true).'</pre>';
