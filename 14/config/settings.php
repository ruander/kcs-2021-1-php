<?php
//szolgáltatások
const VALID_SERVICES = [
    0 => [
        'name' => "szauna",
        'price' => 1900
    ],
    1 => [
        'name' => "uszoda",
        'price' => 2900
    ],
    2 => [
        'name' => "wellness (szauna+uszoda)",
        'price' => 3490
    ],
    3 => [
        'name' => "várlátogatás kisérővel",
        'price' => 11900
    ],
    4 => [
        'name' => "borkostolás",
        'price' => 2700
    ],
    5 => [
        'name' => "mosoda",
        'price' => 900
    ]
];//szolgáltatások segédtömb (erőforrás)
