<?php
//Erőforrások
require_once "../config/database.php";//db csatlakozás ($link)
require_once "../config/functions.php";// _once csak akkor tölti be ha még nem volt betöltve
require_once "../config/settings.php";//beállítások betöltése
if(!isset($output)){//védelem önálló futtatás ellen, ha megnézünk 'valamit' pl output változó ami az indexben van deklarálva és nincs, akkor irány az index
    header('location:http://szobafoglalas.local');
    exit();
}

//url paraméterek
$action = filter_input(INPUT_GET, 'action') ?: 'read';//mit szeretnék csinálni? ha nem tudjuk, listázás
$tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT) ?: null;//melyik elemmel szeretnénk csinálni, ha nincs akkor null vagy [ID]

//POST feldolgozás
if (!empty($_POST)) {
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //hibakezelés
    $hiba = [];

    //név nem lehet üres
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//html elemek eltávolítása
    $name = trim($name);//space és egyéb alap elemek(\r\n...) eltávolítása
    if ($name == '') {
        $hiba['name'] = '<span class="error">Kötelező mező!</span>';
    }

    //email kötelező és email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {//ha nem jó email formátum
        $hiba['email'] = '<span class="error">Érvénytelen formátum!</span>';
    }
    //telefon, kötelező (text vagy tel)
    //@TODO ha nem küldted, HF!

    //érkezés,indulás (éjszakák száma)
    //érkezés hiba, ha ...
    //korábbi, mint az aktuális nap (ma)
    $arrival_date = filter_input(INPUT_POST, 'arrival_date');//éééé-hh-nn
    //dátum ellenőrzése 1: dátum-e?
    //kapott 'szöveg' timestampre(int) alakítása

    $now = date('Y-m-d');//ma

    //dátum ellenőrzése 2: dátum-e?
    $date_elements = explode('-', $arrival_date);
    //var_dump($date_elements);
    if (
        count($date_elements) < 3 //elvileg ilyen nem lehet (form manipulálás)
        ||
        !checkdate($date_elements[1], $date_elements[2], $date_elements[0])
        ||
        $now > $arrival_date
    ) {
        $hiba['arrival_date'] = '<span class="error">Hibás adat (minimum mai nap)!</span>';
    }
    //távozás dátuma leave_date legyen legalább 1 nappal nagyobb mint az arrival_date
    $leave_date = filter_input(INPUT_POST, 'leave_date');//éééé-hh-nn
    if ($leave_date <= $arrival_date) {
        $hiba['leave_date'] = '<span class="error">Hibás adat (minimum érkezés +1 nap)!</span>';
    }
    //felnőttek, gyerekek száma kötelező, minimum 1 felnőtt
    $options = [
        'options' => [
            'min_range' => 1
        ]
    ];
    $adults = filter_input(INPUT_POST, 'adults', FILTER_VALIDATE_INT, $options);
    if (!$adults) {
        $hiba['adults'] = '<span class="error">Hibás adat (minimum 1 felnőtt)!</span>';
    }
    //gyermekek száma
    $children = filter_input(INPUT_POST, 'children', FILTER_VALIDATE_INT);
    if (
        !$children && $children !== 0 //nem sikerült leszűrni
        ||
        $children < 0 //vagy kisebb mint 0
    ) {
        $hiba['children'] = '<span class="error">Hibás adat!</span>';
    }

//ellátás kötelező (select-option)
//@TODO ha nem küldted, HF! - mardjon kiválasztva ami ki volt, hiba esetén (checked alapján optionbe 'selected')

//szolgáltatások rendberakása
    $services = filter_input(INPUT_POST, 'services', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    //echo '<pre>' . var_export($services, true) . '</pre>';

//terms kötelező elfogadni
    $terms = filter_input(INPUT_POST, 'terms');
    if (!$terms) {
        $hiba['terms'] = '<span class="error">Kötelező bejelölni!</span>';
    }

//ha nincs hiba
    if (empty($hiba)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'arrival_date' => $arrival_date,
            'leave_date' => $leave_date,
            'adults' => $adults,
            'children' => $children,
            'services' => implode(',',array_keys($services))
        ];
        /*echo '<pre>' . var_export(
            implode(',' ,array_keys($data['services']))
            , true
            ) . '</pre>';*/
        //mikor történt a regisztráció
        $time_created = date("Y-m-d H:i:s");//php.net! (2021-04-29 19:32:15)
        //beletesszük a rendezett adataink közé:
        $data['time_created'] = $time_created;

        //echo '<pre>' . var_export($data, true) . '</pre>';
        //mentés adatbázis rekordként
        $qry = "INSERT INTO 
    `reservations` ( 
                    `name`, 
                    `email`, 
                    `phone`, 
                    `arrival_date`, 
                    `leave_date`, 
                    `adults`, 
                    `children`, 
                    `provision`, 
                    `services`, 
                    `time_created`) 
    VALUES (     
            '{$data['name']}', 
            '{$data['email']}', 
            '...később', 
            '{$data['arrival_date']}', 
            '{$data['leave_date']}', 
            '{$data['adults']}', 
            '{$data['children']}', 
            '...később', 
            '{$data['services']}', 
            '{$data['time_created']}')";
        //query futtatása!!!
        mysqli_query($link, $qry) or die(myslqi_error($link));

        //átirányítunk üres formra
        header('location:'.$_SERVER['PHP_SELF']);
        exit();//állj , ugyanaz mint a die
    }
}





//műveletek szétválasztása
switch ($action) {
    case 'delete':
        //echo 'törlünk:'.$tid;
        if($tid){
            mysqli_query($link,"DELETE FROM reservations WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
        }
        header('location:'.$_SERVER['PHP_SELF']);
        exit();
        break;

    case 'update':
        echo 'módosítunk:'.$tid;
        break;

    case 'create':
        $title = '<h2>Új felvitel - <a href="index.php">mégse</a></h2>';//űrlap címe + 'vissza' gomb
        $form = $title;
        $form .= '<form method="post">';
//név
        $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" value="' . getValue('name') . '" placeholder="John Doe">' . getError('name') . '          
          </label>';
//email
        $form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" value="' . getValue('email') . '" placeholder="email@you.com">' . getError('email') . '          
          </label>';

//érkezés/távozás @todo HF: hogy tudunk pirossal jelölni, vagy inaktívvá tenni egyes napokat
        $form .= '<label>
            <span>Érkezés<sup>*</sup></span>
            <input type="date" name="arrival_date" value="' .
            (getValue('arrival_date') ?: date('Y-m-d'))
            . '">' . getError('arrival_date') . '</label>';

        $form .= '<label>
            <span>Távozás<sup>*</sup></span>
            <input type="date" name="leave_date" value="' . getValue('leave_date') . '">' . getError('leave_date') . '</label>';

//felnőttek/gyerekek száma
        $form .= '<label>
            <span>Felnőttek száma<sup>*</sup></span>
            <input type="text" name="adults" value="' . getValue('adults') . '" placeholder="2">' . getError('adults') . '          
          </label>';

        $form .= '<label>
            <span>Gyermekek száma 6-18 éves korig</span>
            <input type="text" name="children" value="' . getValue('children') . '" placeholder="2">' . getError('children') . '
            </label>';

//szolgáltatások

        $form .= '<div class="services">';
        $form .= '<h3>Válasszon a szolgáltatásaink közül</h3>';

////segédtömb($services) bejárásával
        $services = filter_input(INPUT_POST, 'services', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        foreach (VALID_SERVICES as $serviceId => $service) {
            //short if
            $checked = is_array($services) && array_key_exists($serviceId, $services) ? 'checked' : '';

            $form .= '<label>
    <input type="checkbox" name="services[' . $serviceId . ']" value="1" ' . $checked . '> 
    ' . $service['name'] . ' 
    <span class="price">(' . $service['price'] . '.-HUF)</span>
    </label>';
        }


        $form .= '</div>';

//checkbox - adatvédelmi
        $form .= '<label class="terms">
            <input type="checkbox" name="terms" value="1">
            <span>' . getError('terms') . ' Elolvastam és megértettem az <a href="#" target="_blank">Adatvédelmi Tájékoztató</a>ban leírtakat!</span>
          </label>';

        $form .= '<button>küldés</button></form>';

        $output .= $form;//ez legyen a kimenete a filenak amit majd ki akarunk írni
        break;

    default:
        //listázás
        $qry = "SELECT id, name, phone, arrival_date, adults, children FROM reservations";//kérés kialakítása
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));//lekérés

        $table = '<div><a href="?action=create">új felvitel</a></div>
          <table border="1">';
//címsor
        $table .= '<tr>
              <th>id</th>
              <th>név</th>
              <th>tel</th>
              <th>érkezés</th>
              <th>felnőtt/gyerek</th>
              <th>művelet</th>
            </tr>';
        while ($row = mysqli_fetch_row($result)) {
            $table .= '<tr>
                  <td>' . $row[0] . '</td>
                  <td>' . $row[1] . '</td>
                  <td>' . $row[2] . '</td>
                  <td>' . $row[3] . '</td>
                  <td>' . $row[4] . '/' . $row[5] . '</td>
                  <td><a href="?action=update&amp;tid=' . $row[0] . '">módosít</a> | <a href="?action=delete&amp;tid=' . $row[0] . '">töröl</a></td>
                </tr>';
            //@todo HF: mielőtt a törlés linket meghívjuk, confirm delete
            //echo '<pre>'.var_export($row,true).'</pre>';//kibontás ciklusban
        }
        $table .= '</table>';
//kiírás 1 lépésben

        $output .= $table;//ez legyen a kimenete a filenak amit majd ki akarunk írni
        break;

}

//echo $output;//ott írjuk ki, ahol az include lesz
