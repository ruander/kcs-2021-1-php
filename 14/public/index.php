<?php
require "../config/database.php";//db csatlakozás ($link)
require "../config/functions.php";// _once csak akkor tölti be ha még nem volt betöltve
require "../config/settings.php";//beállítások betöltése
//erőforrások
$output = '';//ide gyüjtjük a kiírandó elemeket
//stílusok, ideiglenesen itt...
$style = '<style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
form {
    max-width:400px;
    margin: 0 auto;
    display:grid;
    flex-direction: column;
}
label {
    display:flex;
    flex-direction: column;
    margin: 15px 0;
}
label.terms {
    flex-direction:row;
}
.error {
    color:#f00;
    font-style:italic;
    font-size:.8em;
}
</style>';

//CRUD betöltése
include "szobafoglalasok.php";//CRUD logika + output kialakítása

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Szobafoglalás</title>
    <?php echo $style;//stílusok kiírása ?>
</head>
<body>
<section>
    <?php
    //output kiírása
    echo $output;
    ?>
</section>
</body>
</html>
